from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from time import sleep
import smtplib, ssl

def main():
    # Driver creation
    options=webdriver.ChromeOptions()
    options.binary_location = '/usr/bin/chromium'
    options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=options)
    # Getting to the grade posting website with login
    try:
        driver.get('https://avcid.avc.edu/_layouts/PG/login.aspx?ReturnUrl=%2fcas%2flogin%3fservice%3dhttps%253A%252F%252Fmyavc.avc.edu%252Fc%252Fportal%252Flogin&service=https%3A%2F%2Fmyavc.avc.edu%2Fc%2Fportal%2Flogin')
        username = driver.find_element_by_id("frmLogin_UserName")
        password = driver.find_element_by_id("frmLogin_Password")
        username.send_keys("myavclogin")
        password.send_keys("myavcpassword")
        driver.find_element_by_id("btnLogin").click()
        try:
            myElem = WebDriverWait(driver, 120).until(EC.presence_of_element_located((By.CLASS_NAME, "active")))
        except TimeoutException:
            print("didn't werk timeout")
            driver.quit()
            exit(1)
        driver.get('https://myavc.avc.edu/web/home/register-pay')
        try:
            myElem = WebDriverWait(driver, 120).until(EC.presence_of_element_located((By.LINK_TEXT, "Final")))
        except TimeoutException:
            print("didn't werk timeout")
            driver.quit()
            exit(1)
        driver.find_element_by_link_text("Final").click()
        gradeTab = driver.window_handles[1]
        driver.switch_to.window(gradeTab)
        posted = False
        # The WAIT IS PAINFUL
        while not posted:
            try:
                myElem = WebDriverWait(driver, 120).until(EC.presence_of_element_located((By.CLASS_NAME, "pagebodydiv")))
            except TimeoutException:
                print("didn't werk timeout")
                driver.quit()
                exit(1)
            posted = len(driver.find_elements_by_class_name("infotext")) == 0
            print(posted)
            if not posted:
                sleep(60)
                driver.refresh()
        elements = driver.find_elements_by_class_name("dddefault")
        grades = [ x.text for x in elements if len(x.text)==1 ]

        # Notification
        import gi
        gi.require_version('Notify', '0.7')
        from gi.repository import Notify
        Notify.init("Grades are posted")
        notify = Notify.Notification.new("Grades are Posted", "Check em {}".format(' '.join(grades)), "You should check your final grades")
        notify.show()

        # Email Message to self
        smtp_server = "smtp.gmail.com"
        port = 587 
        sender_email = "Email@gmail.com"
        password = "App password"
        context = ssl.create_default_context()
        try:
            server = smtplib.SMTP(smtp_server,port)
            server.ehlo()
            server.starttls(context=context)
            server.ehlo()
            server.login(sender_email, password)
            server.sendmail(sender_email, sender_email, "Grades were posted check em {}".format(' '.join(grades)))
        except Exception as e:
            print(e)
        finally:
            server.quit()
    except Exception as e:
        print(e)
    finally:
        driver.quit()

if __name__ == '__main__':
    main()
